## Income and Expense App

This web application or app is built using Streamlit framework. The users' input are saved in csv file. There are three main window tabs, the Dashboard, Income and Expense.
Summary in net worth are shown in Dashboard along with the income and expense tables. The income tab allows the users to input their income while the expense tab allows the users to input their expenses.

![](https://coderlegion.com/?qa=blob&qa_blobid=7643289601212058959)

You may also refer to an article on [How to create an income and expense app in streamlit](https://coderlegion.com/52/how-to-create-an-income-and-expense-app-in-streamlit) in coderlegion for more information about this web app.

## Setup

Install the dependency with

```
pip install -r requirements.txt
```
