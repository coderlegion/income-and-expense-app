"""Income and expense App"""

import streamlit as st
import pandas as pd

INCOME_CSV_FILE = 'income.csv'
EXPENSE_CSV_FILE = 'expense.csv'

def get_income_df():
    try:
        return pd.read_csv(INCOME_CSV_FILE)
    except FileNotFoundError:
        return pd.DataFrame(columns=['Date', 'Amount', 'Note'])

def get_expense_df():
    try:
        return pd.read_csv(EXPENSE_CSV_FILE)
    except FileNotFoundError:
        return pd.DataFrame(columns=['Date', 'Amount', 'Note'])

st.markdown('# Income and Expense App')

dashboard, income, expense = st.tabs(['DASHBOARD', 'INCOME', 'EXPENSE'])

with dashboard:
    df_income = get_income_df()
    df_expense = get_expense_df()

    total_income = df_income['Amount'].sum()
    total_expense = df_expense['Amount'].sum()

    net = total_income - total_expense

    color = 'red' if net < 0 else 'green'
    st.markdown(f'''# Net Worth: <span style='color:{color}'>{net}</span>''', unsafe_allow_html=True)

    col1, col2 = st.columns(2)

    with col1:
        st.write('Income')
        st.dataframe(get_income_df(), height=150)

    with col2:
        st.write('Expense')
        st.dataframe(get_expense_df(),height=150)

with income:
    df = get_income_df()

    with st.form('income'):
        date = st.date_input('Date')
        amount = st.number_input('Amount', value=0.0)
        note = st.text_input('Note')
        submit = st.form_submit_button('Save', type='primary')

    if submit:
        data = {'Date': date, 'Amount': amount, 'Note': note}
        df = pd.concat([df, pd.DataFrame([data])], ignore_index=True)
        df.to_csv(INCOME_CSV_FILE, index=False)
        st.rerun()

    st.dataframe(df)


with expense:
    df = get_expense_df()

    with st.form('expense'):
        date = st.date_input('Date')
        amount = st.number_input('Amount', value=0.0)
        note = st.text_input('Note')
        submit = st.form_submit_button('Save', type='primary')

    if submit:
        data = {'Date': date, 'Amount': amount, 'Note': note}
        df = pd.concat([df, pd.DataFrame([data])], ignore_index=True)
        df.to_csv(EXPENSE_CSV_FILE, index=False)
        st.rerun()

    st.dataframe(df)
